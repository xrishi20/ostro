const _ = require("lodash");
const plugin = require("tailwindcss/plugin");
const defaultTheme = require("tailwindcss/defaultTheme");

function font_size(min, max) {
  const scale = ((max - 16) / 1792) * 100;

  return `clamp(${min / 16}rem, calc(1rem + ${scale}vw), ${max / 16}rem)`;
}

module.exports = {
  content: ["./*.html"],
  theme: {
    screens: {
      sm: "480px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
      xxl: "1600px",
      xxxl: "1920px",

      // Max
      // "max-xs": { max: "359px" },
      // "max-sm": { max: "479px" },
      // "max-md": { max: "767px" },
      // "max-lg": { max: "1023px" },
      // "max-xl": { max: "1279px" },
      // "max-xxl": { max: "1599px" },
      // "max-xxxl": { max: "1919px" },
    },
    transitionDuration: {
      DEFAULT: "300ms",
      150: "150ms",
      200: "200ms",
    },
    transitionTimingFunction: {
      DEFAULT: "cubic-bezier(0.4, 0, 0.2, 1)",
      linear: "linear",
      in: "cubic-bezier(0.4, 0, 1, 1)",
      out: "cubic-bezier(0, 0, 0.2, 1)",
      "in-out": "cubic-bezier(0.4, 0, 0.2, 1)",
    },
    extend: {
      colors: {
        sand: "#FCF7F3",
        ocean: "#225B74",
        dirt: "#A59F97",
        pink: {
          50: "#FCF8F7",
        },
        black: "#121212",
        chalk: "#F4F2F3",
        blue: "#225B74",
        blush: {
          50: "#F8EEE8",
          DEFAULT: "#F2DED1",
        },
        dune: "#A59F97",
      },
      fontFamily: {
        serif: ["Grafier", ...defaultTheme.fontFamily.serif],
      },
      lineHeight: {
        0: "0",
      },
      maxHeight: {
        0: "0",
      },
      spacing: {
        120: "30rem",
        144: "36rem",
        "-px": "-1px",
      },
      gridColumn: {
        "span-13": "span 13 / span 13",
        "span-14": "span 14 / span 14",
        "span-15": "span 15 / span 15",
        "span-16": "span 16 / span 16",
        "span-17": "span 17 / span 17",
        "span-18": "span 18 / span 18",
        "span-19": "span 19 / span 19",
        "span-20": "span 20 / span 20",
        "span-21": "span 21 / span 21",
        "span-22": "span 22 / span 22",
        "span-23": "span 23 / span 23",
        "span-24": "span 24 / span 24",
      },
      gridColumnEnd: {
        13: "13",
        14: "14",
        15: "15",
        16: "16",
        17: "17",
        18: "18",
        19: "19",
        20: "20",
        21: "21",
        22: "22",
        23: "23",
        24: "24",
      },
      gridColumnStart: {
        13: "13",
        14: "14",
        15: "15",
        16: "16",
        17: "17",
        18: "18",
        19: "19",
        20: "20",
        21: "21",
        22: "22",
        23: "23",
        24: "24",
      },
      gridTemplateColumns: {
        22: "repeat(22, minmax(0, 1fr))",
        24: "repeat(24, minmax(0, 1fr))",
      },
    },
  },
  corePlugins: {
    container: false,
    clear: false,
    float: false,
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/aspect-ratio"),

    // Hocus
    plugin(function ({ addVariant, e }) {
      addVariant("hocus", ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) =>
          ["hover", "focus"]
            .map(
              (variant) => `.${e(`hocus${separator}${className}`)}:${variant}`
            )
            .join(",")
        );
      });

      // TODO: Investigate why group-hocus won't work with `@apply`
      addVariant("group-hocus", ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) =>
          ["hover", "focus"]
            .map(
              (variant) =>
                `.group:${variant} .${e(`group-hocus${separator}${className}`)}`
            )
            .join(",")
        );
      });
    }),

    /*
			Responsive type
			@min: @max * .666666 (rems)
			@val: @max / 1792 * 100 (viewport widths)
			@max: scale value (rems)
		*/
    plugin(function ({ addUtilities }) {
      const typeUtilities = {
        // 14
        ".type-sm": {
          fontSize: font_size(12, 14),
        },
        // 18
        ".type-lg": {
          fontSize: font_size(16, 18),
        },
        // 20
        ".type-20": {
          fontSize: font_size(16, 20),
        },
        // 24
        ".type-xl": {
          fontSize: font_size(20, 24),
        },
        // 32
        ".type-2xl": {
          fontSize: font_size(24, 32),
        },
        // 40
        ".type-3xl": {
          fontSize: font_size(28, 40),
        },
        // 48
        ".type-4xl": {
          fontSize: font_size(32, 48),
        },
        // 64
        ".type-5xl": {
          fontSize: font_size(40, 64),
        },
        // 72
        ".type-6xl": {
          fontSize: font_size(48, 72),
        },
        // 96
        ".type-7xl": {
          fontSize: font_size(64, 96),
        },
        // 144
        ".type-10xl": {
          fontSize: font_size(84, 144),
        },
      };

      addUtilities(typeUtilities, ["responsive"]);
    }),

    // Flood utilities
    plugin(function ({ addUtilities, config }) {
      const containerPadding = {
        min: "1rem",
        xs: "2rem",
        sm: "3rem",
        md: "4rem",
        lg: "6rem",
        xl: "8rem",
        xxl: "8rem",
        xxxl: "8rem",
      };

      const floodUtilities = _.map(containerPadding, (padding, screen) => {
        let width = config(`theme.screens.${screen}`);

        return {
          [`@media (min-width: ${width})`]: {
            ".container": {
              "padding-left": padding,
              "padding-right": padding,
            },
            ".flood-r": {
              "margin-right": `-${padding}`,
            },
            ".flood-l": {
              "margin-left": `-${padding}`,
            },
            ".flood-l-none": {
              "margin-left": `0`,
            },
            ".flood-r-none": {
              "margin-right": `0`,
            },
            ".conatiner-l": {
              "padding-left": padding,
            },
            ".conatiner-r": {
              "padding-right": padding,
            },
            ".bordered": {
              "padding-left": `calc(${padding} - 2rem)`,
              "padding-right": `calc(${padding} - 2rem)`,
            },
          },
        };
      });

      floodUtilities.unshift({
        ".container": {
          "padding-left": containerPadding.min,
          "padding-right": containerPadding.min,
        },
        ".flood-r": {
          "margin-right": `-${containerPadding.min}`,
        },
        ".flood-l": {
          "margin-left": `-${containerPadding.min}`,
        },
      });

      addUtilities([...floodUtilities]);
    }),
  ],
};
